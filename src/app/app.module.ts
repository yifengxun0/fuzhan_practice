import { ProductsModule } from './products/products.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductsComponent } from './products/products.component';
import { ContactComponent } from './contact/contact.component';
import { AboutComponent } from './about/about.component';
import { CarComponent } from './products/car/car.component';
import { ElectronicComponent } from './products/electronic/electronic.component';
import { IndustryComponent } from './products/industry/industry.component';
import { FurnitureComponent } from './products/furniture/furniture.component';
import { OthersComponent } from './products/others/others.component';
import { HttpClientModule } from '@angular/common/http';
import { AngularSvgIconModule } from 'angular-svg-icon';
import {  MatIconModule } from "@angular/material/icon";
import {  MatButtonModule} from '@angular/material/button';


@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    ContactComponent,
    AboutComponent,
    CarComponent,
    ElectronicComponent,
    IndustryComponent,
    FurnitureComponent,
    OthersComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ProductsModule,
    HttpClientModule,
    AngularSvgIconModule.forRoot(),
    MatIconModule,
    MatButtonModule,
    BrowserAnimationsModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
