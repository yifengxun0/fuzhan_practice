import { animate, group, query, state,style, transition, trigger } from '@angular/animations';

export const slideInAnimation =  trigger('routeAnimations', [
transition('* => *',[
  style({
    position:'relative'
  }),

  query(':enter, :leave',[
    style({
      position:'absolute',
      top:0,
      left:0,
      width:'100%',
      height:'100%'
    })
  ],{optional: true}),

  query(':enter',[
    style({opacity:0
    }),
    animate(400,style({
      opacity:1
    }))
  ],{ optional: true }),

    query(':leave',[
      style({
        opacity:1,
      }),
      animate(400,style({
        opacity:0,
      }))
    ],{ optional:true }),
  ])
])
