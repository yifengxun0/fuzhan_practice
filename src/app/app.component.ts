import { slideInAnimation } from './animations';
import { Component } from '@angular/core';
import { MatIconRegistry } from "@angular/material/icon";
import { DomSanitizer } from "@angular/platform-browser";
import { RouterOutlet } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations:[
    slideInAnimation
  ]
})
export class AppComponent {

  prepareRoute(outlet: RouterOutlet){
    console.log(outlet)
    return outlet &&
    outlet.activatedRouteData &&
    outlet.activatedRouteData.animation;

  }

  title = 'fuzhan';
  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer
  ){
    this.matIconRegistry.addSvgIcon(
      "close2",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/close2.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "teemo",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/teemosvg.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "close18",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/close_white_18dp.svg")
    );
  }
}
