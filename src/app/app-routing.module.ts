import { AboutComponent } from './about/about.component';
import { AppComponent } from './app.component';
import { ContactComponent } from './contact/contact.component';
import { ProductsComponent } from './products/products.component';
import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [

  { path:"products",pathMatch:'full', component: ProductsComponent, data:{ animation:'two'} },
  { path:"contact", pathMatch:'full',component: ContactComponent, data:{ animation:'three'} },
  { path:"about", pathMatch:'full',component: AboutComponent, data:{ animation:'one'} },
  { path:"", pathMatch:'full' ,component: AboutComponent, data:{ animation:'zero'} },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
