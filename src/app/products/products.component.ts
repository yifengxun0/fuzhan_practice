import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { slideInAnimation } from '../animations';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
  animations:[
    slideInAnimation
  ]
})
export class ProductsComponent implements OnInit {
  car = null;

  prepareRoute(outlet: RouterOutlet){
    console.log(outlet)
    return outlet &&
    outlet.activatedRouteData &&
    outlet.activatedRouteData.animation;

  }

  constructor(){}
  ngOnInit(): void {
}}
