import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CarComponent } from './car/car.component';
import { ElectronicComponent } from './electronic/electronic.component';
import { FurnitureComponent } from './furniture/furniture.component';
import { IndustryComponent } from './industry/industry.component';
import { OthersComponent } from './others/others.component';


const routes: Routes = [
  { path:"car",pathMatch:'full',component: CarComponent, data:{ animation:'car'} },
  { path:"electronic",pathMatch:'full',component: ElectronicComponent, data:{ animation:'electronic'}},
  { path:"industry",pathMatch:'full',component: IndustryComponent, data:{ animation:'industry'} },
  { path:"furniture",pathMatch:'full',component: FurnitureComponent, data:{ animation:'furniture'} },
  { path:"others",pathMatch:'full',component: OthersComponent, data:{ animation:'others'} },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
